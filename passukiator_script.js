function insertAtCaret(el, text)
{
    var caretPos = el.selectionStart;
    var textAreaTxt = el.value;
    el.value = textAreaTxt.substring(0, caretPos) + text + textAreaTxt.substring(caretPos);
    caretPos += text.length;
    document.getElementById("text1").focus();
    el.setSelectionRange(caretPos,caretPos);
}
//credit for insertAtCaret(): https://stackoverflow.com/a/39787460
function iac(t)
{
    insertAtCaret(document.getElementById("text1"), t);
}
function addTaam(el, taam)
{
    var text = el.value;
    var newCaretPos = text.indexOf(" ");
    newCaretPos--;
    el.selectionStart = newCaretPos;
    iac(taam);
}
function addTaam(el, index, taam)
{
    var text = el.value;
    var newCaretPos = text.indexOf(" ", index);
    newCaretPos--;
    el.selectionStart = newCaretPos;
    iac(taam);
    el.selectionStart++;
}
function addSequence(el, sequence) //sequence is string[]
{
    var text = el.value;
    var originalCaretPos = el.selectionStart;
    var newCaretPos = text.indexOf(" ", el.selectionStart);
    cutText = text.substring(newCaretPos,text.length)
    var wordcount = (cutText.split(" ")).length;
    if (wordcount < sequence.length)
        return;
for(var i=0; i<sequence.length; i++)
{
    if (i+1!=sequence.length)
    {
        addTaam(el, newCaretPos, sequence[i]);
        newCaretPos = text.indexOf(" ", text.indexOf(" ", newCaretPos)+1);
        }
        else
        {
        if (wordcount > sequence.length)
        {
            addTaam(el, newCaretPos, sequence[i]);
        }
        else
        {
            el.selectionStart = text.length+1;
            iac(sequence[i]);
        }
    }
}
}
function addS(sequence)
{
    addSequence(document.getElementById("text1"), sequence);
    document.getElementById("text1").focus();
    }
function showOption(checkboxID, optionID)
{
    var checkbox = document.getElementById(checkboxID);
    var option = document.getElementById(optionID);
    if (checkbox.checked)
    {
        option.style.display = "block";
        option.focus();
    }
    else
        option.style.display = "none";
}
function fontChange(font)
{
    document.getElementById("text1").style.fontFamily = "\"" + font + "\", serif";
}
function ltr()
{
    document.getElementById("text1").style.direction = "ltr";
}
function rtl()
{
    document.getElementById("text1").style.direction = "rtl";
}
function bold(str)

{
    if (str=="true")
        document.getElementById("text1").style.fontWeight = "bold";
    if (str=="false")
        document.getElementById("text1").style.fontWeight = "normal";
}
function color(str)
{
    if (str=="default")
    {
        document.getElementById("text1").style.color = "#0000bb";
        document.getElementById("text1").style.backgroundColor = "white";
    }
    if (str=="dark")
    {
        document.getElementById("text1").style.color = "aliceblue";
        document.getElementById("text1").style.backgroundColor = "darkslategray";
    }
    if (str=="parchment")
    {
        document.getElementById("text1").style.color = "black";
        document.getElementById("text1").style.backgroundColor = "#ffffee";
    }
}
function addAutoSequence(arr)
  {
      var words = arr.split(' ');
      var wc = words.length;
      const sequence = chooseAutoSequence(wc);                   
      addTaamToWords(words, sequence);
      var rstr = "";
      for(var i=0; i<words.length; i++)
      {
          if (i != words.length-1)
              rstr += words[i] + " ";
          else
              rstr += words[i] + ": ";
      }
      return rstr;
  }
function addTaamToWords(words, sequence)
{
    for(var i=0; i < sequence.length; i++)
    {
        var str = words[i];
        words[i] = str.substring(0, str.length-1) + sequence[i] + str[str.length-1];
    }
}
function chooseAutoSequence(num)
{
    if (num==null)
        return;
    if (num==0)
        return [];
    num--;
    const seq1 = [['ֽ']];
    const seq2 = [['֖','ֽ']];
    const seq3 = [['֥','֖','ֽ'], ['֖','֥','ֽ']];
    const seq4 = [['֥','֖','֥','ֽ'],['֖','֥','֖','ֽ']];
    const seq5 = [['֥','֖','֑','֖','ֽ'], ['֛','֥','֖','֥','ֽ'], ['֣','֔','֥','֖','ֽ']];
    const seq6 = [['֥','֖','֣','֑','֖','ֽ'], ['֙','֔','֖','֑','֖','ֽ'], ['֗','֛','֥','֖','֥','ֽ']];
    const seq7 = [['֗','֛','֥','֖','֑','֖','ֽ'],['֥','֖','֑','֥','֖','֥','ֽ'],['֛','֥','֖','֑','֥','֖','ֽ'],['֛','֥','֖','֑','֖','֥','ֽ']];
    const seq8 = [['֥','֖','֣','֑','֥','֖','֣','ֽ'], ['֥','֛','֖','֥','֑','֥','֖','ֽ'], ['֧','֛','֖','֑','֛','֥','֖','ֽ'],['֕','֥','֖','֑','֥','֖','֣','ֽ'],['֥','֖','֑','֗','֛','֥','֖ ','ֽ']];
    const seq9 = [['֥','֖','֣','֑','֙','֣','֔','֖','ֽ'], ['֖','֥','֑','֤','֙','֔','֥','֖','ֽ'],['֧','֛','֖','֥','֑','֥','֖','֥','ֽ']];
    const seq10 = [['֤','֙','֣','֔','֥','֖','֑','֥','֖','ֽ'], ['֤','֙','֣','֔','֥','֖','֑','֖','֥','ֽ'], ['֥','֖','֑','֞','֤','֙','֔','֖','֥','ֽ']];
    const seq11 = [['֤','֙','֣','֔','֥','֖','֣','֑','֥','֖','ֽ'], ['֤','֙','֣','֔','֥','֖','֣','֑','֖','֥','ֽ'],['֕','֖','֥','֑','֙','֜','֧','֛','֖','֥','ֽ']];
    const seq12 = [['֤','֙','֣','֔','֥','֖','֣','֑','֥','֖','֥','ֽ'],['֣','֔','֕','֥','֖','֣','֑','֛','֥','֖','֥','ֽ']];
    const seq13 = [['֠','֙','֧','֛','֥','֖','֥','֑','֕','֥','֖','֥','ֽ'],['֩','֙','֜','֣','֗','֧','֛','֥','֖','֑','֥','֖','ֽ']];
    const seq14 = [['֞','֣','֗','֙','֣','֔','֕','֥','֖','֑','֥','֖','֥','ֽ'],['֥','֖','֑','֣','֗','֤','֙','֣','֔','֥','֖','֥','ֽ']];
    const seq15 = [['֠','֨','֧','֛','֖','֥','֑','֤','֙','֣','֔','֥','֖','֥','ֽ'],['֥','֖','֣','֑','֣','֗','֤','֙','֣','֔','֕','֥','֖','֥','ֽ']];
    const seq16 = [['֠','֨','֧','֛','֥','֖','֣','֑','֤','֙','֣','֔','֥','֖','֥','ֽ'],['֥','֖','֣','֑','֣','֗','֤','֙','֣','֔','֧','֛','֥','֖','֥','ֽ']];
    const seq17 =[['֣','֗','֙','֤','֙','֣','֔','֕','֥','֖','֣','֑','֛','֥','֖','֥','ֽ'],['֣','֮','֣','֔','֣','֣','֗','֤','֙','֣','֔','֖','֑','֥','֖','֥','ֽ']];

    const sequences = [seq1,seq2,seq3,seq4,seq5,seq6,seq7,seq8,seq9,seq10,seq11,seq12,seq13,seq14,seq15,seq16]
    const ran = ( Math.random()*(sequences[num].length-1)).toFixed();
    return sequences[num][ran];
}
function autoTrope(el)
{
    document.getElementById("text-output").innerHTML = "";
    var text = document.getElementById(el);
    var arr = text.value.split('. ');
    var str = "";
    for(var i=0; i<arr.length; i++)
    {
        if (arr[i]!=null)
        {
              str+=addAutoSequence(arr[i]);
        }
        document.getElementById("text-output").innerHTML = str;
    }
}        
function copy(el) 
{
  var copyText = document.getElementById(el);
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */
  navigator.clipboard.writeText(copyText.value);
}
